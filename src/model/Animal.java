package model;

public class Animal extends SerVivo{
	//Atributos
	private char colunaVertebrada;

	public Animal(String reino){
		super(reino);
	}
	public Animal(String reino, char colunaVertebrada){
		super(reino);
		this.colunaVertebrada = colunaVertebrada;
	}
	public char getColunaVertebrada() {
		return colunaVertebrada;
	}
	public void setColunaVertebrada(char colunaVertebrada) {
		this.colunaVertebrada = colunaVertebrada;
	}

}
