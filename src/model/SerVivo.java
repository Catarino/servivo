package model;

import java.util.ArrayList;

public class SerVivo {

	//Atributos

	private ArrayList<SerVivo> listaSerVivo;
	private String reino;
	
	//M�todos

	public SerVivo(String reino){
		listaSerVivo = new ArrayList<SerVivo>();
		this.reino = reino;
	}
	public String getReino() {
		return reino;
	}
	public void setReino(String reino) {
		this.reino = reino;
	}
}
